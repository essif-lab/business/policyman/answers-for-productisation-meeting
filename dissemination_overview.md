# Dissemination Overview

This is not a formal deliverable, but it helps the eSSIF-Lab consortium to make
the joint dissemination effort visible. Please add your eSSIF-Lab related dissemination
efforts (publications, presentations, …) to this table:

| Date          | Title         | Author           | URL              |
| ------------- | -------------:| ----------------:| ----------------:|
|               |               |                  |                  |
|               |               |                  |                  |
|               |               |                  |                  |
