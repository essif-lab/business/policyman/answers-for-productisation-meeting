# BOC-1 - Collaboration with SSI business ecosystem

## The needs of the Policy Management project

<!-- Describe what minimum inputs would you need from the other eSSIF-Lab subgrantees of eSSIF-Lab
- Please check the other eSSIF-Lab subgrantees’ solutions descriptions, e.g. to see what they can provide to your benefit.
- Please contact and communicate with other eSSIF-Lab subgrantees as you see fit.
- Specify the eSSIF-Lab subgrantees or other parties (EU-NGI, US-DHS-SVIP, cross-border, other) that you want to collaborate with, and that you are already working with.
- Identify the  eSSIF-Lab subgrantees and other parties would you want to perform interoperability tests with, and what the nature of such tests would be.
-->

What we need from the other projects are:
- exemplar policies, so as to ensure that our policy language is capable of catering for all their verifier/service provider requirements.

We would also benefit from:
- a privacy and security review of our architecture by the Nym project (Harry Halpin)
- interworking tests of our VP with other projects that use JWT proofs


## What <!--our project--> may provide

<!-- Advertise the things that you think may be of benefit to other subgrantees. Preferably, be concrete in your offers and specify the specific subgrantee(s) you target with that offer.
-->

A multi-lingual policy management GUI and supporting infrastructure to allow business managers to set their requirements for Verifiable Credentials. Our CNL policy will allow selective disclosure statements to be provided so as to conform to GDPR.

## Anything else <!--our project--> needs to say

We have agreed the following joint texts with other participants as below


### 1. With Enrico.talin@commerc.io of CommercioKYC

COMMERCIOKYC  Verifiablecredentials.info  COLLABORATION PROPOSAL
MDCommons

    VCL will provide their Policy Management infrastructure to CommericioKYC

    CommercioKYC will provide its KYC to VCL

AGREEMENT

We Both Agree that we are willing to collaborate during the ESSIF by exchanging this email message

### 2. With Nick Mayne of Resonate

Verifiable Credentials Ltd and Resonate have agreed to collaborate in
Phase 2 of the project by each being users of the others deliverables.

Verifiable Credentials Ltd will integrate Resonate's OIDC SIOP service
and protocols into its web browser application layer

Resonate will integrate Verifiable Credentials Ltd application
independent VC middleware and policy management infrastructure into its
into its Community Credentials plugin and infrastructure 

### 3. With meri.seistola@wellbeingcart.com of Wellbeing 

Wellbeing and Verifiable Credentials Ltd have agreed to collaborate in Phase 2 by each being the users of the others deliverables, in the following way:

Wellbeing will integrate VCLs policy management CNL GUI and server into its Data as Currency application. VCL will provide easy to use APIs for this

VCL will integrate WB's OCA into its policy management schema handling. WB will provide easy to use APIs for this. 

### 4. With Irene Adamski <irene@jolocom.com> of Jolocom

Verifiable Credentials Ltd is interested in implementing the universal wallet backup service being provided by Jolocom

Jolocom in interested in implementing the Policy Management infrastructure of Verifiable Credentials Ltd.

Both companies agree that easy to use APIs should be made available to the other so as to ease the burden of implementation.

### 5. With Harry Halpin <harry@nymtech.net> of Nymtech

Verifiable Credentials Ltd and Nymtech have agreed to collaborate as follows

Nymtech will work with VCL to perform a security and privacy evaluation of VCLs background and foreground technology, and VCL will correct any vulnerabilities or significant weaknesses that are identified

Nymtech will integrate VCLs multilingual policy management GUI



