# PolicyMan Project Summary

For more information, please contact: 

Em Prof David W Chadwick

CEO, Verifiable Credentials Ltd

Email: d.w.chadwick@verifiablecredentials.info

skype: davidwchadwick

LinkedIn: https://www.linkedin.com/in/davidwchadwick/ 


## Introduction

### About Verifiable Credentials Ltd (VCL)

VCL is a spin-out from the University of Kent. VCL has implemented a complete fully functional SSI infrastructure (Identiproof™). This is an application independent verifiable credentials middleware that provides:

i) a VC Issuer that issues standard conformant verifiable credentials, using JWT proofs.

ii) a VC Holder app and wallet that issues standard conformant verifiable presentations, using JWT proofs. Each VP contains selectively disclosed VCs that satisfy the Service Provider's policy for access to its protected resource. No additional PII is released, ensuring that the SP conforms to GDPR.

iii) a VC Verifier that verifies a presented VP against the SP's verification/presentation policy and confirms that
- all the VCs belong to the same user, i.e. attribute aggregation. The user is identified by a transient public key.
- the holder is this user, by proving possession of the corresponding private key used to sign the VP, and
- all the VCs have been issued by trusted issuers and only contain the attributes they are trusted to issue.
- the set of VCs match the SP's presentation policy and the holder should be granted access to the protected resource.

iv) a Policy Registry, which stores the machine processable presentation policies of SPs.

v) three APIs: one for an IDP to communicate with the VC Issuer, one for an SP to communicate with the VC Verifier, and one for an application client to communicate with the VC Holder.


### PolicyMan

The PolicyMan project has supplemented the SSI infrastructures by providing:

a) a multi-lingual policy management infrastructure for creating, updating, storing and deleting the SP's semantic validation policies in controlled natural language (CNL);

b) multi-lingual CNL policies that can be written in either conjunctive normal form (CNF) or disjunctive normal form (DNF) so that they are able to cater for any conceivable policy requirements of the SP;

c) a CNL policy database server to hold these CNL semantic validation policies;

d) a policy translation tool to convert the CNL verification policies into a machine processable presentation policy in the language understood by the Identiproof™ SSI infrastructure, for storing in the Policy Registry, ready for retrieving by its verifiers and holders;

e) an initial design of a universal SSI verification system that will allow any SP to communicate with any SSI holder, to receive a VP in response that satisfies its verification/presentation policy, and have the VP verified by a Simple Universal Verifier (SUV) that can cater for any type of VP encoding.

If we are successful in progressing to phase 3, then we will provide an open source implementation of the Simple Universal Verifier.

## Summary

### Business Problem

How do businesses who wish to implement SSI:

a) define the policies they wish to use for controlling access to their resources?

b) decide which SSI infrastructure(s) to use or support, given that their are over 70 different DID methods that have already been registered, meaning that users may posses one of 70 (or more) different VC wallets and apps?

We will help businesses to solve these problems by providing business owners with:

a) a user-friendly, multilingual, browser-based interface for specifying their validation policies at the semantic level in natural language that they can easily understand;

b) a simple universal verification infrastructure that can accept verifiable presentations (VPs) from any type of SSI implementation, confident that these VPs conform to their verification policies.



### Technical Solution

VCL's technology will: 

a) simplify the task of resource (SP) administrators semantically creating their various validation policies for access to their various resources, by using controlled natural language;

b) allow interworking between different SSI infrastructures that support different cryptographic proof formats, different VP structures, and different presentation policy languages;

c) provide plug in components for other SSI vendors, regardless of their underlying technologies and infrastructures;

d) be modular, with defined APIs between the different services, thereby allowing all eSSIF participants to choose which components of the policy management and universal verification infrastructure they require.



## Integration with the eSSIF-Lab Functional Architecture and Community

### CNL APIs

The controlled natural language (CNL) database holds three types of data:
- VC Profiles
- Selective Disclosure Statements, and
- CNL policies.

Each VC Profile is identified by a Profile Name (string) e.g. profileName= “residency permit".

Each SD Statement is identified by a SD Name (string) e.g. sDName = “Prove Age”

Each CNL validation policy is identified by a unique policyMatch parameter. This is either a type (string), or an action (string) on a target resource (string) e.g. {type=“Over 18”}; {action=“Read”, target=“CV”}; {action=“Download”, target=“Document X”} etc. Such a policy identifier is easy to remember and use by the managers who create their validation policies. This is encoded in JSON as the policyMatch object.

The CNL database server supports the following API calls (functions) for each type of data:
- Create: create a CNLpolicy|VCprofile|SDstatement entry in the CNL database
- Read: return a CNLpolicy|VCprofile|SDstatement from the CNL database
- Update: update a CNLpolicy|VCprofile|SDstatement in the CNL database
- Delete: delete a CNLpolicy|VCprofile|SDstatement from the CNL database
- Search: search for a CNLpolicy|VCprofile|SDstatement in the CNL database

The database server runs on the manager's computer (local host) and is accessed via a browser. The CNL language is determined by the language setting of the browser.

### Policy Registry APIs

The Policy Registry holds machine processable presentation policies in different machine processable formats. Each format can be evaluated by the VC Holders and VC Verifiers of a particular SSI infrastructure. A policy conversion service reads CNL validation policies from the CNL database, converts them into the presentation policies of the SSI infrastructure(s) it supports, and inserts them into the Policy Registry.

The Policy Registry supports two API calls (functions): 

- Insert a Presentation Policy and
- Retrieve a Presentation Policy. 

Each presentation policy has a unique PolicyMatch parameter that identifies the matching policy in the Policy Registry.


### Simple Universal Verifier APIs

The SUV has two APIs, an Access Decision API and a PolicyRef API. 
The Access Decision API is used by the SP to validate the received VP. 
The PolicyRef API is used to get a pointer to where the presentation policy (in the wallet's syntax) can be found.

### The PolicyRef API

This is the interface to the SUV's database. Its purpose is to find the reference to the validation policy (in the Wallet's supported local syntax).

There is only one API call:

- GetPolicyRef, which has the following input arguements: the URL of the SP's resource, and a list of supported policy syntaxes/types.
- the response comprises: The URL of the Policy Registry where the policies are stored, and the PolicyMatch parameter that is needed to retreive the policy.

### Access Decision API

This API is the interface between a Service Provider's application layer code and the Simple Universal Verifier of the VC middleware. Its purpose is to tell the application layer whether the received VP (in whatever format it might be) matches the presentation policy for this resource.

There is only one call: 

- Decision Request, which has the following input arguments: the VP provided by the Holder, the URL of the SP's resource, and the Challenge that was given to the Holder for it to include in the returned VP.

- The Decision Response has the following parameters: Granted, along with the Validated Attributes of the holder, or Denied with either a reason and/or any validated attributes of the holder.

### Verifier API

This is the interface between the SUV and the SSI Verifier. The SUV presents a Verifiable Presentation, and receives back the Authenticated Verifiable Credentials and the Challenge embedded in the VP.


There is only one API call:

VerifyVP, which has the following input arguments: a VP in JWT format
the response comprises: the challenge embedded in the VP and the set of verified credentials

Note. We propose to migrate from the above Verifier API to the W3C Verifier API during phase 3 of the project.

